import 'dart:convert';

import 'package:dio/dio.dart';

class WeatherApi {
  final dio = Dio();
  
 
  Future<Map<String, dynamic>> request() async {
    Response response;
    var apiKey = 'CBRLKNA4LapN2KDwE0eMT83nyhzT8rIj';
String latitude = '40.75872069597532';
  String longitude = '-73.98529171943665'.toString();
  //tring link =
      //"https://api.tomorrow.io/v4/timelines?location=$latitude,$longitude&fields=temperature_2m,humidity_2m,precipitation,wind_speed&apikey=$apiKey";


    response = await dio.get('https://api.tomorrow.io/v4/weather/forecast?location=42.3478,-71.0466&apikey=CBRLKNA4LapN2KDwE0eMT83nyhzT8rIj');
    //print(response.data.toString());
    // The below request is the same as above.
    //response = await dio.get(
    //'/test',
    //queryParameters: {'id': 12, 'name': 'dio'},
    //);
    print(response.statusCode);
    if (response.statusCode == 200) {
      //print(response.data.toString());
      final actualData =
          jsonDecode(response.toString()) as Map<String, dynamic>;
      //print(response.data.runtimeType);
      return actualData;
    } else {
      throw Exception('Connection Error');
    }
  }
}
