import 'dart:async';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod/riverpod.dart';
import 'package:wheater_study/whetherApi.dart';

final apiprovider = Provider<WeatherApi>((ref) => WeatherApi());
void main(List<String> args) {
  runApp(ProviderScope(child: Wheather()));
}

final dataprovider = FutureProvider<Map<String, dynamic>>((ref) {
  return ref.read(apiprovider).request();
});

class Wheather extends ConsumerWidget {
  Wheather({super.key});
  

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userData = ref.watch(dataprovider);
    return MaterialApp(
      theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(
              seedColor: Color.fromARGB(255, 127, 250, 164)),
          useMaterial3: true),
      home: SafeArea(
          child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Weather With Reverpod',
            style: TextStyle(fontSize: 25),
          ),
        ),
        body: Column(
          children: [
            Text('Weather Data'),
            SizedBox(
              height: 20,
            ),
            userData.when(
              data: (data) {
                final g = data['timelines']['minutely'];
                print(g);
                return Expanded(
                  child: ListView.separated(itemBuilder: (context, index) {
                    final h  = g[index];
                    return Text(h.toString());
                  }, separatorBuilder: (context, index) => Divider(), itemCount: g.length),
                );
              },
              error: (error, stackTrace) {
                
                return Text(error.toString());
              },
              loading: () {
                return Center(child: CircularProgressIndicator());
              },
            )
          ],
          
        ),
      )),
    );
  }
}
